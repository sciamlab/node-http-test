//===============================================================================

var start2 = process.hrtime();
const request = require('request');
request('https://sciamlab.com', (err, res, body) => {
  if (err) { return console.log(err); }
  //console.log(body);
  hrend2 = process.hrtime( start2);
  console.log("NODE REQUEST: %ds %dms",hrend2[0],hrend2[1]/1000000);
});

//===============================================================================

const axios = require('axios');
var start3 = process.hrtime();
axios.get('https://sciamlab.com')
  .then(response => {
    //console.log(response.data);
    hrend3 = process.hrtime( start3);
    console.log("AXIOS HTTP: %ds %dms",hrend3[0],hrend3[1]/1000000);
  })
  .catch(error => {
    console.log(error);
  });

//==============================================================================

const superagent = require('superagent');
//console.log("SUPERAGENT HTTP START: ",new Date());
var start4 = process.hrtime();
superagent.get('https://sciamlab.com')
.end((err, res) => {
  if (err) { return console.log(err); }
  //console.log(res.body);
  hrend4 = process.hrtime( start4);
  console.log("SUPERAGENT HTTP: %ds %dms",hrend4[0],hrend4[1]/1000000);
});


//=======================================================================

const https = require('https');
var start1 = process.hrtime();
//console.log("NODE HTTP START: ",new Date());
https.get('https://sciamlab.com', (resp) => {
  let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    //console.log(data);
    //console.log("NODE HTTP END:",new Date());
    hrend1 = process.hrtime( start1);
    console.log("NODE HTTP: %ds %dms",hrend1[0],hrend1[1]/1000000);
  });

}).on("error", (err) => {
  console.log("Error: " + err.message);
});


//=============================================================================

const { Client } = require('undici')
const client = new Client(`https://sciamlab.com`)
var start6 = process.hrtime();

client.request({
  path: '/',
  method: 'GET'
}, function (err, data) {
  if (err) {
    // handle this in some way!
    return
  }

  const {
    statusCode,
    headers,
    trailers,
    body
  } = data

//  console.log('response received', statusCode)
//  console.log('headers', headers)

  body.setEncoding('utf8')
  body.on('data', ()=>{})
  body.on('end', () => {    
    //console.log('trailers', trailers)
    hrend6 = process.hrtime( start6);
    console.log("NODE UNDICI: %ds %dms",hrend6[0],hrend6[1]/1000000);
  })

  client.close()
})


//===============================================================================


const got = require('got');

var start5 = process.hrtime(); 
//console.log("GOT HTTP START: ",new Date());
got('https://sciamlab.com').then(response => {
  //console.log(response);
  hrend5 = process.hrtime( start5);
  console.log("GOT HTTP: %ds %dms",hrend5[0],hrend5[1]/1000000);
}).catch(error => {
  console.log(error);
});
